#pragma once

#include <string>
#include <vector>
#include <map>
#include <memory>

#include "DoubleSpreadsheetCell.h"
#include "StringSpreadsheetCell.h"
#include "BoolSpreadsheetCell.h"

using namespace std;

enum class DataType {STRING, INTEGER, DOUBLE, BOOLEAN};

class Table {
private: 
  vector < vector< unique_ptr<SpreadsheetCell> > > mdata;
  vector <DataType> mtypes; 
  vector <string> mcolumnNames; 
  map<string, size_t> mcolumn2Index; // to speed up the mapping from column name to column index
  string mname; 
  static size_t mcolumnWidth ;

private: // helper function 
  void appendDataType ( const string  & s ) ;
  void appendDataType (DataType t) ;

  bool insertRow (const vector<string>&v ) ;
  void deleteColumn (size_t column ) ;

public:

  // cstr
  Table () = default ;
  inline const string& getName () const { return mname; } ;
  Table (const string& , size_t, const vector<string> &, const vector<string> & ) ;
  
  bool insertRow ( const string& line ) ;
  void deleteRow (size_t row ) ;

  void join ( const string& cname1, const string& cname2, const unique_ptr<Table> & t2 ) ;

  void insertColumn ( const string &type, const string& name ) ;
  void deleteColumn ( const string & name  ) ;


  void displayAll () const ;
};


