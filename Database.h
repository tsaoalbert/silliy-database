#pragma once

#include <string>
#include <vector>
#include <memory>
#include "Table.h"

class Database {

private:
  vector< unique_ptr<Table> > mtables;
  string mname;
public:

public:
  Database () = default;
  Database ( const string& name) ;

  virtual ~Database () ;

  unique_ptr<Table>& createTable  ( Table* );

  unique_ptr<Table>& createTable  (
      const string& name, 
      size_t numRows, 
      const vector <string > &, 
      const vector <string > & 
  ) ;
  const unique_ptr<Table>& createTable  ( const vector<string>& ) ;
  void deleteColumnFrom ( const vector<string>& tokens) ;

  void executePrintFrom ( const vector<string>& tokens) ;
  void executeInsertRowInto ( ifstream& fin, const vector<string>& ) ;

  inline const auto & getTables(size_t i) const { return mtables[i]; };
  inline auto & getTables(size_t i) { return mtables[i]; };

  void displayAll ( ) const ;
  
  inline auto& findTable (const string& name ) const {
    for ( auto& t1: mtables ) {
      if ( t1->getName() == name ) return t1;
    }
    return mtables.front();
  }
  void join ( const vector<string>& ) ;

};
