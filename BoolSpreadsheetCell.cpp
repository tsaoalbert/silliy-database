#include <iostream>
#include <sstream>
#include <limits>

#include "BoolSpreadsheetCell.h"

using namespace std;

// cstr
BoolSpreadsheetCell::BoolSpreadsheetCell() : mValue(false)
{
}

void   BoolSpreadsheetCell::set(const string& inString)
{
	mValue = stringToBool(inString) ;
}

string   BoolSpreadsheetCell::getString() const
{
	return boolToString(mValue);
}

string   BoolSpreadsheetCell::boolToString(bool inValue)
{
	return inValue? "true": "false" ;
}

bool BoolSpreadsheetCell::stringToBool(const string& inValue)
{
	return inValue == "true";
}
