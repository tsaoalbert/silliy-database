/*
CREATE cs445class 3 string string bool emotion person Y/N
INSERT INTO cs445class 8 ROWS
*/

#include <iostream>
#include <fstream>


#include "Database.h"

using namespace std;

// cstr
Database::Database ( const string& name) : mname(name)
{
}

// dstr
Database::~Database ( ) 
{
}

void Database::executePrintFrom ( const vector<string>& tokens) {
}

void Database::executeInsertRowInto ( ifstream& fin, const vector<string>& tokens) {
  const string& tableName = tokens[3];
  auto& t1 = findTable ( tableName );
  int n = stoi(tokens[4]);
  for (size_t i=0;  i < n ; ++i ) {
    string line ;
    getline( fin , line) ;
    t1->insertRow ( line );
  }
}



void Database::deleteColumnFrom ( const vector<string>& tokens) { 
  const string& tableName = tokens[3];
  auto& t1 = findTable ( tableName );

  for (size_t i = 4; i < tokens.size(); ++i ) {
    const string& columnName = tokens[i];
    // cout << columnName << endl;
    t1->deleteColumn ( columnName ); // delete column by name
  }
}

std::unique_ptr<Table>&  
Database::createTable  (
  const std::string& name, size_t numRows, 
  const std::vector <std::string > & types, 
  const std::vector <std::string > & columnNames
) {

  Table* t1 = new Table (name, numRows,types, columnNames);
  return createTable  ( t1 );
}

std::unique_ptr<Table>& Database::createTable  ( Table* table) 
{
  mtables.emplace_back ( table );
  return mtables.back();
}

const unique_ptr<Table>& Database::createTable( const vector<string>& tokens)            
{
  const string& tableName = tokens[2];
  int n = stoi(tokens[3]);
 
  vector<string> colNames ;
  vector<string> types ;
 
  for (size_t i = 0 ; i < n; ++i  ) {
    types.push_back ( tokens[4+i] ) ;
    colNames.push_back ( tokens[4+n+i] ) ;
  }
  return createTable  (tableName, 0,types, colNames);
}




  void Database::displayAll ( ) const 
  {
    std::cout << std::endl;
    std::cout << "Database: " << mname << std::endl;
    for (auto& t: mtables )  {
      t->displayAll(); 
    }
    std::cout << std::endl;
  }

/*
      JOIN TABLE myFRuits AND unitPrices WHERE
            name = species AND PRINT 3 name 1 color  2  price/oz 3
*/
  void Database::join ( const vector<string>& tokens ) {
    const string& tname1 = tokens[2];
    const string& tname2 = tokens[4];

    const string& cname1 = tokens[6];
    const string& cname2 = tokens[8];

    auto& t1 = findTable ( tname1 );
    auto& t2 = findTable ( tname2 );
    t1->join ( cname1, cname2, t2 );
}
