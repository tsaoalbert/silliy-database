#pragma once

#include "SpreadsheetCell.h"
#include "DoubleSpreadsheetCell.h"
using namespace std;

class StringSpreadsheetCell : public SpreadsheetCell
{
public:
	StringSpreadsheetCell();
	StringSpreadsheetCell( const string &) ;

  // convert input string to individual data type
	virtual void set(const string& inString) override;
	virtual string getString() const override;

	auto get () const { return mValue; } // 

private:
	string mValue;
};
