#pragma once

#include "SpreadsheetCell.h"
#include <string>

using namespace std;

class   BoolSpreadsheetCell : public SpreadsheetCell
{
public:
	BoolSpreadsheetCell(); // default cstr

	BoolSpreadsheetCell (const string& s) { set(s); } ; // cstr

  // convert input string to individual data type
	virtual void set(const string& inString) override;

	virtual string getString() const override;

	auto get () const { return mValue; } // 

private:
	static string boolToString(bool inValue);
	static bool stringToBool(const string& inValue);

	bool mValue; // type bool
};
