#pragma once

#include "SpreadsheetCell.h"
using namespace std;

class DoubleSpreadsheetCell : public SpreadsheetCell
{
public:
	DoubleSpreadsheetCell(); // default cstr
	DoubleSpreadsheetCell(const string &); // cstr

  // convert input string to individual data type
	virtual void set(const string& inString) override;
	virtual string getString() const override;

	bool get () const { return mValue; } // 

private:
	static string doubleToString(double inValue);
	static double stringToDouble(const string& inValue);

private:
	double mValue; // type double
};
