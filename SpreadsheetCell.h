#pragma once

#include <string>
using namespace std;

class SpreadsheetCell
{
  public:

	SpreadsheetCell() = default ; // cstr
	SpreadsheetCell(const string& ) {} ; // cstr
	virtual ~SpreadsheetCell() = default; // dstr

  // convert input string to individual data type
	virtual void set(const string& ) = 0; 

  // convert individual data type to string 
	virtual string getString() const = 0; 

};
