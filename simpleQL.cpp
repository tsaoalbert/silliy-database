/*
CREATE cs445class 3 string string bool emotion person Y/N
INSERT INTO cs445class 8 ROWS
*/
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <memory>

#include "Table.h"
#include "Database.h"

using namespace std;

template <class T>
T operator+(const T& lhs, const T& rhs)
{
	T o; 
	o.set(lhs.get() + rhs.get());
	return o ; 
}

// TODO: finish all the unfinished database script commands 
void executeCommandScript ( Database& db, const string& fn) {
  ifstream fin (fn);
  string line ;
  cout << left << endl;
  while ( getline( fin , line) ) // get a line from the fin
  {
    istringstream iss( line );
    vector<string> tokens {istream_iterator<string>{iss}, istream_iterator<string>{}};
    size_t n = tokens.size();
    if ( n < 1 || tokens[0][0] == '#' ) continue;
    if ( tokens[0] == "QUIT" ) {
      break;
    }
    if (n < 2 )  continue;

    string key = tokens[0]+tokens[1] ;
    if ( key == "CREATETABLE" ) {
      db.createTable ( tokens) ;
    } else if ( key == "PRINTFROM" ) {
      cout << "ERROR: unrecognized command syntax: " << line << endl;
      db.executePrintFrom ( tokens) ; // to be finished
    } else if ( key == "INSERTROW" ) {
      db.executeInsertRowInto ( fin, tokens) ;
      /*  DELETE COLUMN FROM myFruits color  */
    } else if ( key == "DELETECOLUMN" ) {
      db.deleteColumnFrom ( tokens) ;
    } else if ( key == "JOINTABLE" ) {
      db.join ( tokens );
    // } else if ( key == "SORTTABLE" ) {
      // example SORT TABLE myFruits BY COLUMN color 
    } else if ( key == "PRINTTABLE" ) {
      const string& tname1 = tokens[2];
      auto& t1 = db.findTable ( tname1 );
      t1->displayAll ();
    } else if ( key == "PRINTDATABASE" ) {
      db.displayAll ();
    } else {
      cout << "ERROR: unrecognized command syntax: " << line << endl;
    }
  }
  fin.close();

} 

int main()
{
  Database db ("db1" );
  string fn = "test-1-table-commands.txt";

  executeCommandScript (db, fn);

}

