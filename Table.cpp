/*
CREATE cs445class 3 string string bool emotion person Y/N
INSERT INTO cs445class 8 ROWS
*/


#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>

#include "Table.h"

using namespace std;

size_t Table::mcolumnWidth = 10;

  ostream &operator<< (ostream &out, const SpreadsheetCell *r) {
    out << r->getString ();
    return out;
  }

  ostream &operator<< (ostream &out, const unique_ptr<SpreadsheetCell> &r) {
    out << r.get ();
    return out;
  }
  string toString ( DataType t ) {
    vector<string> msg = {"STRING", "INTEGER", "DOUBLE", "BOOLEAN"};
    return  msg [static_cast<size_t> (t)];
  }
  void Table::appendDataType ( const string & s ) {
    if ( s=="string") {
      appendDataType (DataType::STRING);
    } else if ( s=="double" ) {
      appendDataType (DataType::DOUBLE);
    } else if ( s=="int" ) {
      appendDataType (DataType::INTEGER);
    } else if ( s=="bool" ) {
      appendDataType (DataType::BOOLEAN);
    } else {
    }
  }

  void Table::appendDataType (DataType t) {
    mtypes.push_back ( t );
  }

  void Table::join ( const string& cname1, const string& cname2, const unique_ptr<Table> & t2 ) {

    size_t i =  mcolumn2Index[cname1] ;
    size_t j =  t2->mcolumn2Index[cname2] ;
    cout << "xxx JOIN Table " << mname << ", column=\" " << cname1 << "\"" << endl;
    cout << "xxx with Table " << t2->mname << ", column=\" " << cname2 << "\"" << endl;
    for ( const auto& r1: mdata ) {
      for ( const auto& r2: t2->mdata ) {
        bool ok =  (r1[i]->getString() == r2[j]->getString()) ;
        if (ok ) {
          for ( const auto& c: r1 ) { cout << setw(mcolumnWidth) << c ; } 
          for ( const auto& c: r2 ) { 
            bool print =  (c->getString() != r2[j]->getString()) ;
            if ( print ) cout << setw(mcolumnWidth) << c ; 
          } 
          cout << endl;
          
         cout << "xxx match" << endl;
        }  
      }
    }
  }


  void Table::displayAll () const {
    cout << endl;
    cout << "Table: " << mname << endl;
    for ( auto& s: mcolumnNames ) {
      cout << setw(mcolumnWidth) << s;
    } 
    cout << endl;
    for ( auto& t: mtypes ) {
      cout << setw(mcolumnWidth) << "---------" ;
    } 
    cout << endl;
    for ( const auto& r: mdata ) {
      for ( const auto& c: r ) {
        cout << setw(mcolumnWidth) << c ;
      }
      cout << endl;
    }
  }
	SpreadsheetCell * createCell (const string& s, DataType type) {
	    SpreadsheetCell *q = nullptr;
      switch (type ) {
        case DataType::STRING:  
	        q = new StringSpreadsheetCell ( s );
          break;
        case DataType::INTEGER:  
	        q = new StringSpreadsheetCell ( s );
          break;
        case DataType::DOUBLE:  
	        q = new DoubleSpreadsheetCell ( s );
          break;
        case DataType::BOOLEAN:
	        q = new BoolSpreadsheetCell ( s );
          break;
      }
    return q;
  }

  Table::Table (const string& name, size_t numRows, 
    const vector<string> &types, const vector<string> &columnNames ) 
    : mname(name)
  {
    for (size_t i=0; i < types.size(); ++i ) {
      insertColumn (types[i], columnNames[i] );
    }
    cout << "Create Table " << name << endl;
  }

  bool Table::insertRow ( const string& line ) {
    istringstream iss( line );
    vector<string> v{istream_iterator<string>{iss}, istream_iterator<string>{}};
    return insertRow ( v );
  }

  void Table::insertColumn ( const string &type, const string& colName ) {
    if ( mcolumn2Index.find ( colName) != mcolumn2Index.end() ) {
      // column exists
      cout << "Cannot add column " << colName << " in Table " << mname << endl;
      return ;
    }
    size_t n = mcolumnNames.size();  
    appendDataType ( type );
    mcolumnNames.push_back( colName ) ; 
    mcolumn2Index[colName] = n;
    for ( auto& row: mdata) {
      string s ;
	    SpreadsheetCell *q = createCell (s, mtypes.back() ) ;
	    row.emplace_back( q );
    }
  }

  // TODO: detect and delete duplicate column
  void Table::deleteColumn ( const string &colName ) {
    if ( mcolumn2Index.find ( colName) == mcolumn2Index.end() ) {
      // detect non-exitent column
      cout << "Cannot find column " << colName << " in Table " << mname << endl;
    } else {
      cout << "Delete column " << colName << " from Table " << mname << endl;
      size_t i =  mcolumn2Index[colName] ;
      cout << "i=" << i << endl;
      mcolumn2Index.erase ( mcolumn2Index.find(colName) );
      deleteColumn (  i );
    }  
  }

  // TODO: insert a new record
  bool Table::insertRow (const vector<string>&v ) {
    size_t i = 0; 
	  mdata.push_back( vector<unique_ptr<SpreadsheetCell>>()  );
    for (const auto& s: v ) {
	    SpreadsheetCell *q = createCell (s, mtypes[i] ) ;
	    mdata.back().emplace_back( q );
      ++i;
    }
    bool ok = ( i == mtypes.size() ) ;
    if ( !ok ) { 
      cerr << "ERROR: insertRow fails!!!" << endl;
      exit(0);
    }
    return ok ;
  }

  // TODO: delete row (record) i
  void Table::deleteRow (size_t i  ) {
    cout << "delete row " <<  i << endl;

    mdata.erase (mdata.begin()+i);
  }

  // TODO: delete column (property) i
  void Table::deleteColumn (size_t i  ) {
    cout << *( mcolumnNames.begin() + i ) << endl ;
    // cout << *( mtypes.begin() + i ) << endl ;

    mtypes.erase ( mtypes.begin() + i ) ;
    mcolumnNames.erase ( mcolumnNames.begin() + i ) ;
    size_t j = 0 ;
    j = 0 ;
    for ( auto& c: mdata) {
      c.erase ( c.begin()+i);
      j++;
    }
  }
